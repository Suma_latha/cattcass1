﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CATTCASS1_1
{
    class Program
    {
        static void Main(string[] args)
        {

            //first creating a variable 
            //it is type string becuase the output is a english sentence
            string stores_sn_sentence;

            //assigning the expected sentence
            stores_sn_sentence = "study nildana";

            //as per the project requirement, displaying the stored value in debug output
            //since we are using debug method for output, output will appear in debug window
            Debug.WriteLine("Main - value of stores_sn_sentence variable is " + stores_sn_sentence);
        }
    }
}
